const fetchRegions = () => {

    const response = fetch('http://localhost:3003/weather')  
    .then(res => res.json());

    return response;

};

const fetchForecast = (region) => {

    const forecast = fetch(`http://localhost:3003/weather`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'region': region
        }),
    }).then(res => res.json());

    return forecast;
}

(async () => {

    const dropdown = document.getElementById('regions');
    const result = await fetchRegions();

    for (let i = 0; i < result.regions.length; i++) {

        let option = result.regions[i];
        let optionElement = document.createElement('option');
        let att = document.createAttribute('value');
        att.value = option;
        optionElement.setAttributeNode(att);
        optionElement.innerText = option;
        dropdown.append(optionElement);
    }
})();

(() => {

    const dropdown = document.getElementById('regions');
    const conditionsInfo = document.getElementById('conditions');
    const tempInfo = document.getElementById('temp');
    const forecastSection = document.getElementsByClassName('forecast')[0];
    const header = forecastSection.querySelector('h3');

    dropdown.addEventListener('change', async () => {

        let forecast = await fetchForecast(dropdown.value);
        header.innerText = dropdown.value;
        tempInfo.innerText = forecast.forecast.temp + '°C';
        conditionsInfo.innerText = forecast.forecast.conditions;
    });
}) ();