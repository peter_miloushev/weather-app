import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
//import * as weatherForecast from './data.json';

const regions = ['Africa', 'Asia', 'Australia', 'Europe', 'North America', 'South America'];
const forecast = {
    Africa: { 
        conditions: 'Sunny weather',
        temp: 29
    },
    Asia: { 
        conditions: 'Rain',
        temp: 22
    },
    Australia: { 
        conditions: 'Warm and nice',
        temp: 27
    },
    Europe: {
        conditions: 'Cloudy',
        temp: 21
    },
    'North America': {
        conditions: 'Hurricanes',
        temp: 25
    },
    'South America': {
        conditions: 'Perfect weather',
        temp: 23
    }
};

const PORT = 3003;
const app = express();
app.use(bodyParser.json());
app.use(cors());

app.get('/weather', (req, res) => {

    res.status(200).send({ regions: regions });
});

app.post('/weather', (req, res) => {

    const data = req.body;

    res.status(200).send({ forecast: forecast[data.region] });
})

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));